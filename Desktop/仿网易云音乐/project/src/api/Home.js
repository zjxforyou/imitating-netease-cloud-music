//引入网络请求文件
import request from '../utils/request' 
//封装一个网络请求方法 	
export const recommendMusic=params=>request({
	url:"/personalized",
	params
})
//获取最新音乐
export const hotMusic=params=>request({
	url:"/personalized/newsong",
	params
})

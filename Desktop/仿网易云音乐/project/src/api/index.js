import {recommendMusic,hotMusic} from '../api/Home'
import {hotSearch,Searchkeyword} from '../api/Search'
import{getSongById,getLyricById} from '../api/Play'
export const recommendMusicAPI=recommendMusic//把网络请求方法拿过来到处
export const hotMusicAPI=hotMusic//把最新音乐的网络请求方法导出来
export const hotSearchAPI=hotSearch//把热搜关键词网络请求方法导出来
export const SearchkeywordAPI=Searchkeyword//把搜索框的请求方法导出来
export const getSongByIdAPI=getSongById
export const getLyricByIdAPI=getLyricById
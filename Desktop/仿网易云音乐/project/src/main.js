import Vue from 'vue'
import App from './App.vue'
import "./mobile/flexible.js"
import "./styles/reset.css"
import router from './router'//導入路由对象'
import {NavBar,Tabbar, TabbarItem,Col, Row,Image as VanImage ,Cell, CellGroup,Icon,Search} from 'vant'


Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(NavBar)
Vue.use(Col);
Vue.use(Row);
Vue.use(VanImage);
Vue.use(Cell);
Vue.use(CellGroup);
Vue.use(Icon)
Vue.use(Search);

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
